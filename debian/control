Source: ffc
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Johannes Ring <johannr@simula.no>,
           Drew Parsons <dparsons@debian.org>,
           Francesco Ballarin <francesco.ballarin@unicatt.it>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-setuptools,
               python3-dijitso,
               python3-fiat,
               python3-ufl-legacy
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/science-team/fenics/ffc
Vcs-Git: https://salsa.debian.org/science-team/fenics/ffc.git
Homepage: https://fenicsproject.org
Rules-Requires-Root: no

Package: python3-ffc
Architecture: all
Depends: python3-dijitso (>= ${fenics:Upstream-Version}),
         python3-dijitso (<< ${fenics:Next-Upstream-Version}),
         python3-fiat (>= ${fenics:Upstream-Version}),
         python3-fiat (<< ${fenics:Next-Upstream-Version}),
         python3-ufl-legacy (>= ${fenics:Upstream-Version}),
         python3-numpy,
         ${python3:Depends},
         ${misc:Depends}
Description: compiler for finite element variational forms (Python 3)
 The FEniCS Form Compiler FFC provides state-of-the-art automatic and
 efficient evaluation of general multilinear forms (variational
 formulations) for FEniCS. FFC functions as the form evaluation
 system for DOLFIN but can also be used to compile forms for other
 systems.
 .
 FFC works as a compiler for multilinear forms by generating code (C
 or C++) for the evaluation of a multilinear form given in
 mathematical notation. This new approach to form evaluation makes it
 possible to combine generality with efficiency; the form can be given
 in mathematical notation and the generated code is as efficient as
 hand-optimized code.
 .
 This package installs the legacy FFC library for Python 3.
 .
 You may want to consider installing python3-ffcx instead to get the
 next-generation FFC-X.
